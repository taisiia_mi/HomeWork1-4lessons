package Exercise1;

import java.util.ArrayList;

public class Name {
   // 1. Написати програму для роботи з списком. У першій половині списку замінити всі входження деякого елементу Е1 на  будь-який інший елемент Е2;

    public static void main(String[] args) {

        ArrayList<String> names = new ArrayList<String>();
        names.add("Anna");
        names.add("Mike");
        names.add("Iryna");
        names.add("Juliia");
        names.add("Victoriia");
        names.add("Mikhael");
        names.add("Vitalii");
        names.add("Artem");
        names.add("Dana");
        names.add("Yana");

        int qty=names.size();
        int pos1=Math.round(names.size()/2-names.size()/4);
        int pos2=Math.round(names.size()/2+names.size()/4);

        System.out.println("Qty in collection:"+String.valueOf(qty));
        String abc = names.get(pos1);
        names.set(pos1, names.get(pos2));
        names.set(pos2, abc);


       for (String people : names) {

          System.out.println(people);

        }
    }
}
