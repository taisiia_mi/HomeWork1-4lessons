package Exercise2;

/**
 2. Задано масив цілих чисел. Вивести масив в оберненому порядку, а потім видалити з нього повторні входження кожного елемента.
 */
public class WorkDay {
  public static void main(String[] args) {
    int[] workDays = {1,1,1,1, 2, 3, 3, 4, 5, 8, 9, 10, 11, 5, 12, 15, 16, 17, 18, 19, 22, 23, 24, 25, 26, 29, 30};
    int n = workDays.length;
    int[] mass = new int[n];

    for (int a = n-1; a >=0; a--) {
      int j;
      for (j = 0; j < a; j++)
        if (workDays[a] == workDays[j])
          break;
      if (a == j) {
        for (int c = 0; c <= n; c++) {
          mass[c] = workDays[a];
          System.out.print(mass[c] + " ");
          break;
        }
      }
    }

  }
}

