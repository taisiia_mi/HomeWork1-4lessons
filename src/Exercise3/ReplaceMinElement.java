package Exercise3;

/**
 3. Найменший елемент масиву замінити цілою частиною середнього арифметичного всіх елементів. Якщо в масиві є декілька найменших елементів, то замінити останній з них.
 */
public class ReplaceMinElement {
  public static void main(String[] args) {
    int[] days = {1, 1, 1, 2, 3, 4, 5, 5, 8, 9, 10, 11, 12, 15, 16, 17, 18, 19, 22, 23, 24, 25, 26, 29, 30};
    int averageArrayPart = 0;
    int min = days[0];
    int sum = 0;
    for (int i = 0; i < days.length; i++)
      if (min > days[i])
        min = days[i];
    System.out.println("Минимальное значение: " + days[min]);

    for (int num : days) {
      sum = sum + num;
      averageArrayPart = Math.round(sum / days.length);
    }
    System.out.println("Среднее арифметическое всех элеметов массива: " + averageArrayPart);
    for (int i = days.length - 1; i>= 0; i--) {
      if (days[i]==min){
        days[i]=averageArrayPart;
        break;
      }
      }
    for (int num : days) {
      System.out.println(num);

    }
  }
}